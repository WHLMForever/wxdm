// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMESUBTITLES_SubtitleDisplaySubsystem_generated_h
#error "SubtitleDisplaySubsystem.generated.h already included, missing '#pragma once' in SubtitleDisplaySubsystem.h"
#endif
#define GAMESUBTITLES_SubtitleDisplaySubsystem_generated_h

#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSubtitleFormat_Statics; \
	static class UScriptStruct* StaticStruct();


template<> GAMESUBTITLES_API UScriptStruct* StaticStruct<struct FSubtitleFormat>();

#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_SPARSE_DATA
#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_RPC_WRAPPERS
#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSubtitleDisplaySubsystem(); \
	friend struct Z_Construct_UClass_USubtitleDisplaySubsystem_Statics; \
public: \
	DECLARE_CLASS(USubtitleDisplaySubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameSubtitles"), NO_API) \
	DECLARE_SERIALIZER(USubtitleDisplaySubsystem)


#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUSubtitleDisplaySubsystem(); \
	friend struct Z_Construct_UClass_USubtitleDisplaySubsystem_Statics; \
public: \
	DECLARE_CLASS(USubtitleDisplaySubsystem, UGameInstanceSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GameSubtitles"), NO_API) \
	DECLARE_SERIALIZER(USubtitleDisplaySubsystem)


#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubtitleDisplaySubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubtitleDisplaySubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubtitleDisplaySubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubtitleDisplaySubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubtitleDisplaySubsystem(USubtitleDisplaySubsystem&&); \
	NO_API USubtitleDisplaySubsystem(const USubtitleDisplaySubsystem&); \
public:


#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubtitleDisplaySubsystem(USubtitleDisplaySubsystem&&); \
	NO_API USubtitleDisplaySubsystem(const USubtitleDisplaySubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubtitleDisplaySubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubtitleDisplaySubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USubtitleDisplaySubsystem)


#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_40_PROLOG
#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_SPARSE_DATA \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_RPC_WRAPPERS \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_INCLASS \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_SPARSE_DATA \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_INCLASS_NO_PURE_DECLS \
	FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMESUBTITLES_API UClass* StaticClass<class USubtitleDisplaySubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Lyra_Plugins_GameSubtitles_Source_Public_SubtitleDisplaySubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
