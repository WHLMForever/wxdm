// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPocketCapture;
#ifdef POCKETWORLDS_PocketCaptureSubsystem_generated_h
#error "PocketCaptureSubsystem.generated.h already included, missing '#pragma once' in PocketCaptureSubsystem.h"
#endif
#define POCKETWORLDS_PocketCaptureSubsystem_generated_h

#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_SPARSE_DATA
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDestroyThumbnailRenderer); \
	DECLARE_FUNCTION(execCreateThumbnailRenderer);


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDestroyThumbnailRenderer); \
	DECLARE_FUNCTION(execCreateThumbnailRenderer);


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPocketCaptureSubsystem(); \
	friend struct Z_Construct_UClass_UPocketCaptureSubsystem_Statics; \
public: \
	DECLARE_CLASS(UPocketCaptureSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketCaptureSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPocketCaptureSubsystem(); \
	friend struct Z_Construct_UClass_UPocketCaptureSubsystem_Statics; \
public: \
	DECLARE_CLASS(UPocketCaptureSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketCaptureSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPocketCaptureSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPocketCaptureSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketCaptureSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketCaptureSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketCaptureSubsystem(UPocketCaptureSubsystem&&); \
	NO_API UPocketCaptureSubsystem(const UPocketCaptureSubsystem&); \
public:


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketCaptureSubsystem(UPocketCaptureSubsystem&&); \
	NO_API UPocketCaptureSubsystem(const UPocketCaptureSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketCaptureSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketCaptureSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPocketCaptureSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_14_PROLOG
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_RPC_WRAPPERS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_INCLASS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_INCLASS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POCKETWORLDS_API UClass* StaticClass<class UPocketCaptureSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketCaptureSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
