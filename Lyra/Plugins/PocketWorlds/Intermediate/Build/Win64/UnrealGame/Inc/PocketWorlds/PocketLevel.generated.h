// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POCKETWORLDS_PocketLevel_generated_h
#error "PocketLevel.generated.h already included, missing '#pragma once' in PocketLevel.h"
#endif
#define POCKETWORLDS_PocketLevel_generated_h

#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_SPARSE_DATA
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_RPC_WRAPPERS
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPocketLevel(); \
	friend struct Z_Construct_UClass_UPocketLevel_Statics; \
public: \
	DECLARE_CLASS(UPocketLevel, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevel)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUPocketLevel(); \
	friend struct Z_Construct_UClass_UPocketLevel_Statics; \
public: \
	DECLARE_CLASS(UPocketLevel, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevel)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPocketLevel(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPocketLevel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevel(UPocketLevel&&); \
	NO_API UPocketLevel(const UPocketLevel&); \
public:


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevel(UPocketLevel&&); \
	NO_API UPocketLevel(const UPocketLevel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevel); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPocketLevel)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_13_PROLOG
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_RPC_WRAPPERS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_INCLASS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_INCLASS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POCKETWORLDS_API UClass* StaticClass<class UPocketLevel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
