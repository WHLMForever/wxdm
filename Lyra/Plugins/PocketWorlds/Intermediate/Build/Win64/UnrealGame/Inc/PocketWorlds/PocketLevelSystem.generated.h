// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POCKETWORLDS_PocketLevelSystem_generated_h
#error "PocketLevelSystem.generated.h already included, missing '#pragma once' in PocketLevelSystem.h"
#endif
#define POCKETWORLDS_PocketLevelSystem_generated_h

#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_SPARSE_DATA
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_RPC_WRAPPERS
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPocketLevelSubsystem(); \
	friend struct Z_Construct_UClass_UPocketLevelSubsystem_Statics; \
public: \
	DECLARE_CLASS(UPocketLevelSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevelSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPocketLevelSubsystem(); \
	friend struct Z_Construct_UClass_UPocketLevelSubsystem_Statics; \
public: \
	DECLARE_CLASS(UPocketLevelSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevelSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPocketLevelSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPocketLevelSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevelSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevelSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevelSubsystem(UPocketLevelSubsystem&&); \
	NO_API UPocketLevelSubsystem(const UPocketLevelSubsystem&); \
public:


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPocketLevelSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevelSubsystem(UPocketLevelSubsystem&&); \
	NO_API UPocketLevelSubsystem(const UPocketLevelSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevelSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevelSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPocketLevelSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_18_PROLOG
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_RPC_WRAPPERS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_INCLASS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_INCLASS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POCKETWORLDS_API UClass* StaticClass<class UPocketLevelSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelSystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
