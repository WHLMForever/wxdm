// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef POCKETWORLDS_PocketLevelInstance_generated_h
#error "PocketLevelInstance.generated.h already included, missing '#pragma once' in PocketLevelInstance.h"
#endif
#define POCKETWORLDS_PocketLevelInstance_generated_h

#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_SPARSE_DATA
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePocketLevelShown); \
	DECLARE_FUNCTION(execHandlePocketLevelLoaded);


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePocketLevelShown); \
	DECLARE_FUNCTION(execHandlePocketLevelLoaded);


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPocketLevelInstance(); \
	friend struct Z_Construct_UClass_UPocketLevelInstance_Statics; \
public: \
	DECLARE_CLASS(UPocketLevelInstance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevelInstance) \
	DECLARE_WITHIN(UPocketLevelSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUPocketLevelInstance(); \
	friend struct Z_Construct_UClass_UPocketLevelInstance_Statics; \
public: \
	DECLARE_CLASS(UPocketLevelInstance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PocketWorlds"), NO_API) \
	DECLARE_SERIALIZER(UPocketLevelInstance) \
	DECLARE_WITHIN(UPocketLevelSubsystem)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPocketLevelInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPocketLevelInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevelInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevelInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevelInstance(UPocketLevelInstance&&); \
	NO_API UPocketLevelInstance(const UPocketLevelInstance&); \
public:


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPocketLevelInstance(UPocketLevelInstance&&); \
	NO_API UPocketLevelInstance(const UPocketLevelInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPocketLevelInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPocketLevelInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPocketLevelInstance)


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_18_PROLOG
#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_RPC_WRAPPERS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_INCLASS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_SPARSE_DATA \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_INCLASS_NO_PURE_DECLS \
	FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> POCKETWORLDS_API UClass* StaticClass<class UPocketLevelInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Lyra_Plugins_PocketWorlds_Source_Public_PocketLevelInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
